#/bin/bash

lastsvnid=`git log master|grep git-svn-id |head -1|awk '{print $2}'`
lastcommit=`git log git-svn --grep=$lastsvnid |head -1 |awk '{print $2}'`
echo "lastcommit is $lastcommit"
echo "update git svn "
up=`git svn fetch |wc -l`

if [ $up -gt 0 ];
then
 echo "merge to master";
 git cherry-pick $lastcommit..git-svn;
fi


